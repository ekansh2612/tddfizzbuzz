
public class FizzBuzz {

	
	public String buzzz(int num) {
		if ( num % 15 == 0) {
			return "fizzbuzz";
		}
		
		if (num % 5 == 0) {
			return "buzz";
		}
		else if (num % 3 == 0) {
			return "fizz";
		}
		
		return num + "";
	}
}
