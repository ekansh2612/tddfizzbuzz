import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FizzBuzzTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testDivisibleBy3() {
		FizzBuzz b = new FizzBuzz();
		String result = b.buzzz(27);
		assertEquals("fizz", result);
	}
	@Test
	public void testDivisibleBy5() {
		FizzBuzz b = new FizzBuzz();
		String result = b.buzzz(15);
		assertEquals("buzz", result);
	}
	
	@Test
	public void testDivisibleBy3and5() {
		FizzBuzz b = new FizzBuzz();
		String result = b.buzzz(45);
		assertEquals("buzz", result);
	}
	@Test
	public void testOtherNum() {
		FizzBuzz b = new FizzBuzz();
		String result = b.buzzz(4);
		assertEquals("4", result);
	}
	@Test
	public void testPrimeNumber() {
		FizzBuzz b = new FizzBuzz();
		String result = b.buzzz(11);
		assertEquals("whizz", result);
	}
}
